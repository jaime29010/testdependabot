package main

import (
	"fmt"
	"github.com/blevesearch/bleve"
	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
)

func main() {
	fmt.Println("Hello world")
	mux.NewRouter()
	httptransport.NewServer(nil, nil, nil, nil)
	bleve.NewIndexMapping()
}